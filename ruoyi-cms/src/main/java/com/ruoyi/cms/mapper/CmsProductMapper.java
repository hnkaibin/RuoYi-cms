package com.ruoyi.cms.mapper;

import java.util.List;
import com.ruoyi.cms.domain.CmsProduct;
import org.apache.ibatis.annotations.Param;
import com.ruoyi.cms.domain.CmsMaterial;

/**
 * 产品Mapper接口
 *
 * @author ruoyi
 * @date 2020-09-12
 */
public interface CmsProductMapper
{
    /**
     * 查询产品
     *
     * @param id 产品ID
     * @return 产品
     */
    public CmsProduct selectCmsProductById(@Param("id") Long id, @Param("siteId") Long siteId);

    /**
     * 查询产品列表
     *
     * @param cmsProduct 产品
     * @return 产品集合
     */
    public List<CmsProduct> selectCmsProductList(CmsProduct cmsProduct);

    /**
     * 新增产品
     *
     * @param cmsProduct 产品
     * @return 结果
     */
    public int insertCmsProduct(CmsProduct cmsProduct);

    /**
     * 修改产品
     *
     * @param cmsProduct 产品
     * @return 结果
     */
    public int updateCmsProduct(CmsProduct cmsProduct);

    /**
     * 删除产品
     *
     * @param id 产品ID
     * @return 结果
     */
    public int deleteCmsProductById(@Param("id") Long id, @Param("siteId") Long siteId);

    /**
     * 批量删除产品
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCmsProductByIds(@Param("ids") String[] ids,  @Param("siteId") Long siteId);
    List<CmsProduct> selectMaterialList(CmsProduct cmsProduct);

    List<CmsMaterial> selectUnMaterialList(CmsProduct cmsProduct);

    void saveMaterial(@Param("id") String id, @Param("arr") String[] arr, @Param("siteId") Long siteId);

    void deleteMaterialBatch(@Param("arr") String[] arr, @Param("id") Long id, @Param("siteId") Long siteId);

}
