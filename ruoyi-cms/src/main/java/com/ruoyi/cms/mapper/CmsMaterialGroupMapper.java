package com.ruoyi.cms.mapper;

import java.util.List;
import com.ruoyi.cms.domain.CmsMaterialGroup;
import org.apache.ibatis.annotations.Param;

/**
 * 素材分组Mapper接口
 * 
 * @author ruoyi
 * @date 2020-09-10
 */
public interface CmsMaterialGroupMapper 
{
    /**
     * 查询素材分组
     * 
     * @param id 素材分组ID
     * @return 素材分组
     */
    public CmsMaterialGroup selectCmsMaterialGroupById(@Param("id") Long id, @Param("siteId") Long siteId);

    /**
     * 查询素材分组列表
     * 
     * @param cmsMaterialGroup 素材分组
     * @return 素材分组集合
     */
    public List<CmsMaterialGroup> selectCmsMaterialGroupList(CmsMaterialGroup cmsMaterialGroup);

    /**
     * 新增素材分组
     * 
     * @param cmsMaterialGroup 素材分组
     * @return 结果
     */
    public int insertCmsMaterialGroup(CmsMaterialGroup cmsMaterialGroup);

    /**
     * 修改素材分组
     * 
     * @param cmsMaterialGroup 素材分组
     * @return 结果
     */
    public int updateCmsMaterialGroup(CmsMaterialGroup cmsMaterialGroup);

    /**
     * 删除素材分组
     * 
     * @param id 素材分组ID
     * @return 结果
     */
    public int deleteCmsMaterialGroupById(@Param("id") Long id, @Param("siteId") Long siteId);

    /**
     * 批量删除素材分组
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCmsMaterialGroupByIds(@Param("ids") String[] ids,  @Param("siteId") Long siteId);
}
