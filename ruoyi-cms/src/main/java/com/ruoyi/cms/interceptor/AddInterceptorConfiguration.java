package com.ruoyi.cms.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class AddInterceptorConfiguration implements WebMvcConfigurer {
    @Autowired
    SiteInterceptor siteInterceptor;
    @Override
    public void addInterceptors(InterceptorRegistry registry){
        registry.addInterceptor(siteInterceptor)
        .addPathPatterns("/cms/**")
        .addPathPatterns("/system/**")
        .addPathPatterns("/login");
    }

}