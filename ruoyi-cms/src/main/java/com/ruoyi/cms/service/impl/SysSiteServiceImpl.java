package com.ruoyi.cms.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.cms.mapper.SysSiteMapper;
import com.ruoyi.cms.domain.SysSite;
import com.ruoyi.cms.service.ISysSiteService;
import com.ruoyi.common.core.text.Convert;

/**
 * 站点Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-09-06
 */
@Service
public class SysSiteServiceImpl implements ISysSiteService 
{
    @Autowired
    private SysSiteMapper sysSiteMapper;

    /**
     * 查询站点
     * 
     * @param id 站点ID
     * @return 站点
     */
    @Override
    public SysSite selectSysSiteById(Long id)
    {
        return sysSiteMapper.selectSysSiteById(id);
    }

    /**
     * 查询站点列表
     * 
     * @param sysSite 站点
     * @return 站点
     */
    @Override
    public List<SysSite> selectSysSiteList(SysSite sysSite)
    {
        return sysSiteMapper.selectSysSiteList(sysSite);
    }

    /**
     * 新增站点
     * 
     * @param sysSite 站点
     * @return 结果
     */
    @Override
    public int insertSysSite(SysSite sysSite)
    {
        sysSite.setCreateTime(DateUtils.getNowDate());
        return sysSiteMapper.insertSysSite(sysSite);
    }

    /**
     * 修改站点
     * 
     * @param sysSite 站点
     * @return 结果
     */
    @Override
    public int updateSysSite(SysSite sysSite)
    {
        sysSite.setUpdateTime(DateUtils.getNowDate());
        return sysSiteMapper.updateSysSite(sysSite);
    }

    /**
     * 删除站点对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSysSiteByIds(String ids)
    {
        return sysSiteMapper.deleteSysSiteByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除站点信息
     * 
     * @param id 站点ID
     * @return 结果
     */
    @Override
    public int deleteSysSiteById(Long id)
    {
        return sysSiteMapper.deleteSysSiteById(id);
    }

    @Override
    public SysSite selectBySitePath(String sitePath) {
        return sysSiteMapper.selectBySitePath(sitePath);
    }

    @Override
    public List<Ztree> selectTree(SysSite sysSite) {
        List<SysSite> sysSites = sysSiteMapper.selectSysSiteList(sysSite);
        List<Ztree> ztrees = new ArrayList<Ztree>();
        for (int i = 0; i < sysSites.size(); i++) {
            Ztree ztree = new Ztree();
            SysSite getSyssite = sysSites.get(i);
            ztree.setId(getSyssite.getId());
            ztree.setpId(0L);
            ztree.setName(getSyssite.getName());
            ztree.setTitle(getSyssite.getName());
            ztrees.add(ztree);

        }
        return ztrees;
    }
}
