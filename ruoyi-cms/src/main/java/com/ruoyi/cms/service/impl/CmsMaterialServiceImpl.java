package com.ruoyi.cms.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.cms.mapper.CmsMaterialMapper;
import com.ruoyi.cms.domain.CmsMaterial;
import com.ruoyi.cms.service.ICmsMaterialService;
import com.ruoyi.common.core.text.Convert;

/**
 * 素材Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-09-10
 */
@Service
public class CmsMaterialServiceImpl implements ICmsMaterialService 
{
    @Autowired
    private CmsMaterialMapper cmsMaterialMapper;

    /**
     * 查询素材
     * 
     * @param id 素材ID
     * @return 素材
     */
    @Override
    public CmsMaterial selectCmsMaterialById(Long id, Long siteId)
    {
        return cmsMaterialMapper.selectCmsMaterialById(id, siteId);
    }

    /**
     * 查询素材列表
     * 
     * @param cmsMaterial 素材
     * @return 素材
     */
    @Override
    public List<CmsMaterial> selectCmsMaterialList(CmsMaterial cmsMaterial)
    {
        return cmsMaterialMapper.selectCmsMaterialList(cmsMaterial);
    }

    /**
     * 新增素材
     * 
     * @param cmsMaterial 素材
     * @return 结果
     */
    @Override
    public int insertCmsMaterial(CmsMaterial cmsMaterial)
    {
        cmsMaterial.setCreateTime(DateUtils.getNowDate());
        return cmsMaterialMapper.insertCmsMaterial(cmsMaterial);
    }

    /**
     * 修改素材
     * 
     * @param cmsMaterial 素材
     * @return 结果
     */
    @Override
    public int updateCmsMaterial(CmsMaterial cmsMaterial)
    {
        cmsMaterial.setUpdateTime(DateUtils.getNowDate());
        return cmsMaterialMapper.updateCmsMaterial(cmsMaterial);
    }

    /**
     * 删除素材对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCmsMaterialByIds(String ids, Long siteId)
    {
        return cmsMaterialMapper.deleteCmsMaterialByIds(Convert.toStrArray(ids), siteId);
    }

    /**
     * 删除素材信息
     * 
     * @param id 素材ID
     * @return 结果
     */
    @Override
    public int deleteCmsMaterialById(Long id, Long siteId)
    {
        return cmsMaterialMapper.deleteCmsMaterialById(id, siteId);
    }
}
