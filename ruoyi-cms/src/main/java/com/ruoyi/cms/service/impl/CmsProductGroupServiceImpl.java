package com.ruoyi.cms.service.impl;

import java.util.List;
import java.util.ArrayList;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.cms.mapper.CmsProductGroupMapper;
import com.ruoyi.cms.domain.CmsProductGroup;
import com.ruoyi.cms.service.ICmsProductGroupService;
import com.ruoyi.common.core.text.Convert;

/**
 * 产品分组Service业务层处理
 *
 * @author ruoyi
 * @date 2020-09-12
 */
@Service
public class CmsProductGroupServiceImpl implements ICmsProductGroupService
{
    @Autowired
    private CmsProductGroupMapper cmsProductGroupMapper;

    /**
     * 查询产品分组
     *
     * @param id 产品分组ID
     * @return 产品分组
     */
    @Override
    public CmsProductGroup selectCmsProductGroupById(Long id, Long siteId)
    {
        return cmsProductGroupMapper.selectCmsProductGroupById(id, siteId);
    }

    /**
     * 查询产品分组列表
     *
     * @param cmsProductGroup 产品分组
     * @return 产品分组
     */
    @Override
    public List<CmsProductGroup> selectCmsProductGroupList(CmsProductGroup cmsProductGroup)
    {
        return cmsProductGroupMapper.selectCmsProductGroupList(cmsProductGroup);
    }

    /**
     * 新增产品分组
     *
     * @param cmsProductGroup 产品分组
     * @return 结果
     */
    @Override
    public int insertCmsProductGroup(CmsProductGroup cmsProductGroup)
    {
        cmsProductGroup.setCreateTime(DateUtils.getNowDate());
        return cmsProductGroupMapper.insertCmsProductGroup(cmsProductGroup);
    }

    /**
     * 修改产品分组
     *
     * @param cmsProductGroup 产品分组
     * @return 结果
     */
    @Override
    public int updateCmsProductGroup(CmsProductGroup cmsProductGroup)
    {
        cmsProductGroup.setUpdateTime(DateUtils.getNowDate());
        return cmsProductGroupMapper.updateCmsProductGroup(cmsProductGroup);
    }

    /**
     * 删除产品分组对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCmsProductGroupByIds(String ids, Long siteId)
    {
        return cmsProductGroupMapper.deleteCmsProductGroupByIds(Convert.toStrArray(ids), siteId);
    }

    /**
     * 删除产品分组信息
     *
     * @param id 产品分组ID
     * @return 结果
     */
    @Override
    public int deleteCmsProductGroupById(Long id, Long siteId)
    {
        return cmsProductGroupMapper.deleteCmsProductGroupById(id, siteId);
    }

    /**
     * 查询产品分组树列表
     *
     * @return 所有产品分组信息
     */
    @Override
    public List<Ztree> selectCmsProductGroupTree(Long siteId)
    {
        CmsProductGroup cmsProductGroupP = new CmsProductGroup();
        if (siteId == 1) {
                cmsProductGroupP.setSiteId(null);
        } else {
                cmsProductGroupP.setSiteId(siteId);
        }

        List<CmsProductGroup> cmsProductGroupList = cmsProductGroupMapper.selectCmsProductGroupList(cmsProductGroupP);
        List<Ztree> ztrees = new ArrayList<Ztree>();
        for (CmsProductGroup cmsProductGroup : cmsProductGroupList)
        {
            Ztree ztree = new Ztree();
            ztree.setId(cmsProductGroup.getId());
            ztree.setpId(cmsProductGroup.getParentId());
            ztree.setName(cmsProductGroup.getGroupName());
            ztree.setTitle(cmsProductGroup.getGroupName());
            ztrees.add(ztree);
        }
        return ztrees;
    }

}
