package com.ruoyi.cms.service;

import java.util.List;
import com.ruoyi.cms.domain.CmsMaterial;

/**
 * 素材Service接口
 * 
 * @author ruoyi
 * @date 2020-09-10
 */
public interface ICmsMaterialService 
{
    /**
     * 查询素材
     * 
     * @param id 素材ID
     * @return 素材
     */
    public CmsMaterial selectCmsMaterialById(Long id, Long siteId);

    /**
     * 查询素材列表
     * 
     * @param cmsMaterial 素材
     * @return 素材集合
     */
    public List<CmsMaterial> selectCmsMaterialList(CmsMaterial cmsMaterial);

    /**
     * 新增素材
     * 
     * @param cmsMaterial 素材
     * @return 结果
     */
    public int insertCmsMaterial(CmsMaterial cmsMaterial);

    /**
     * 修改素材
     * 
     * @param cmsMaterial 素材
     * @return 结果
     */
    public int updateCmsMaterial(CmsMaterial cmsMaterial);

    /**
     * 批量删除素材
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCmsMaterialByIds(String ids, Long siteId);

    /**
     * 删除素材信息
     * 
     * @param id 素材ID
     * @return 结果
     */
    public int deleteCmsMaterialById(Long id, Long siteId);
}
