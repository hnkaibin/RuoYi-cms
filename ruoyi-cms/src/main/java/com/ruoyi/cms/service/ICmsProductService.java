package com.ruoyi.cms.service;

import java.util.List;
import com.ruoyi.cms.domain.CmsProduct;
import com.ruoyi.cms.domain.CmsMaterial;

/**
 * 产品Service接口
 * 
 * @author ruoyi
 * @date 2020-09-12
 */
public interface ICmsProductService 
{
    /**
     * 查询产品
     * 
     * @param id 产品ID
     * @return 产品
     */
    public CmsProduct selectCmsProductById(Long id, Long siteId);

    /**
     * 查询产品列表
     * 
     * @param cmsProduct 产品
     * @return 产品集合
     */
    public List<CmsProduct> selectCmsProductList(CmsProduct cmsProduct);

    /**
     * 新增产品
     * 
     * @param cmsProduct 产品
     * @return 结果
     */
    public int insertCmsProduct(CmsProduct cmsProduct);

    /**
     * 修改产品
     * 
     * @param cmsProduct 产品
     * @return 结果
     */
    public int updateCmsProduct(CmsProduct cmsProduct);

    /**
     * 批量删除产品
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCmsProductByIds(String ids, Long siteId);

    /**
     * 删除产品信息
     * 
     * @param id 产品ID
     * @return 结果
     */
    public int deleteCmsProductById(Long id, Long siteId);


    List<CmsProduct> selectMaterialList(CmsProduct cmsProduct, Long siteId);

    List<CmsMaterial> selectUnMaterialList(CmsProduct cmsProduct, Long siteId);

    void saveMaterial(String id, String materialIds, Long siteId);

    void deleteMaterialBatch(String ids, Long id, Long siteId);

}
