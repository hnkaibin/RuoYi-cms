package com.ruoyi.cms.controller;

import java.util.List;

import com.ruoyi.common.core.domain.Ztree;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.cms.domain.SysSite;
import com.ruoyi.cms.service.ISysSiteService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 站点Controller
 * 
 * @author ruoyi
 * @date 2020-09-06
 */
@Controller
@RequestMapping("/system/site")
public class SysSiteController extends BaseController
{
    private String prefix = "system/site";

    @Autowired
    private ISysSiteService sysSiteService;

    @RequiresPermissions("system:site:view")
    @GetMapping()
    public String site()
    {
        return prefix + "/site";
    }

    /**
     * 查询站点列表
     */
    @RequiresPermissions("system:site:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysSite sysSite)
    {
        startPage();
        List<SysSite> list = sysSiteService.selectSysSiteList(sysSite);
        return getDataTable(list);
    }

    /**
     * 导出站点列表
     */
    @RequiresPermissions("system:site:export")
    @Log(title = "站点", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysSite sysSite)
    {
        List<SysSite> list = sysSiteService.selectSysSiteList(sysSite);
        ExcelUtil<SysSite> util = new ExcelUtil<SysSite>(SysSite.class);
        return util.exportExcel(list, "site");
    }

    /**
     * 新增站点
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存站点
     */
    @RequiresPermissions("system:site:add")
    @Log(title = "站点", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SysSite sysSite)
    {
        return toAjax(sysSiteService.insertSysSite(sysSite));
    }

    /**
     * 修改站点
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        SysSite sysSite = sysSiteService.selectSysSiteById(id);
        mmap.put("sysSite", sysSite);
        return prefix + "/edit";
    }

    /**
     * 修改保存站点
     */
    @RequiresPermissions("system:site:edit")
    @Log(title = "站点", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysSite sysSite)
    {
        return toAjax(sysSiteService.updateSysSite(sysSite));
    }

    /**
     * 删除站点
     */
    @RequiresPermissions("system:site:remove")
    @Log(title = "站点", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(sysSiteService.deleteSysSiteByIds(ids));
    }

    /**
     * 选择站点管理树
     */
    @GetMapping(value = { "/selectWjSiteTree/" })
    public String selectWjSiteTree(ModelMap mmap)
    {
        return prefix + "/tree";
    }

    /**
     * 加载站点管理树列表
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData()
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        sysSite.setName(null);
        List<Ztree> ztrees = sysSiteService.selectTree(sysSite);
        return ztrees;
    }
}
